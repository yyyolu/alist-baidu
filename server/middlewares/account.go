package middlewares

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/yyyolu/alist-baidu/model"
	"gitlab.com/yyyolu/alist-baidu/server/common"
)

func CheckAccount(c *gin.Context) {
	if model.AccountsCount() == 0 {
		common.ErrorStrResp(c, "No accounts,please add one first", 1001)
		return
	}
	c.Next()
}
