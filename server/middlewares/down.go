package middlewares

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/yyyolu/alist-baidu/conf"
	"gitlab.com/yyyolu/alist-baidu/server/common"
	"gitlab.com/yyyolu/alist-baidu/utils"
)

func DownCheck(c *gin.Context) {
	sign := c.Query("sign")
	rawPath := c.Param("path")
	rawPath = utils.ParsePath(rawPath)
	name := utils.Base(rawPath)
	if sign == utils.SignWithToken(name, conf.Token) {
		c.Set("sign", true)
		c.Next()
		return
	}
	pw := c.Query("pw")
	if !common.CheckDownLink(utils.Dir(rawPath), pw, utils.Base(rawPath)) {
		common.ErrorStrResp(c, "Wrong password", 401)
		c.Abort()
		return
	}
	c.Next()
}
