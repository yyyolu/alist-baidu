package middlewares

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/yyyolu/alist-baidu/conf"
	"gitlab.com/yyyolu/alist-baidu/model"
	"gitlab.com/yyyolu/alist-baidu/server/common"
	"gitlab.com/yyyolu/alist-baidu/utils"
)

func PathCheck(c *gin.Context) {
	var req common.PathReq
	if err := c.ShouldBind(&req); err != nil {
		common.ErrorResp(c, err, 400)
		return
	}
	req.Path = utils.ParsePath(req.Path)
	c.Set("req", req)
	token := c.GetHeader("Authorization")
	if token == conf.Token {
		c.Set("admin", true)
		c.Next()
		return
	}
	meta, err := model.GetMetaByPath(req.Path)
	if err == nil {
		if meta.Password != "" && meta.Password != req.Password {
			common.ErrorStrResp(c, "Wrong password", 401)
			c.Abort()
			return
		}
	} else if conf.GetBool("check parent folder") {
		if !common.CheckParent(utils.Dir(req.Path), req.Password) {
			common.ErrorStrResp(c, "Wrong password", 401)
			c.Abort()
			return
		}
	}
	c.Next()
}
