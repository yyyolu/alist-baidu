package controllers

import (
	"gitlab.com/yyyolu/alist-baidu/conf"
	"gitlab.com/yyyolu/alist-baidu/server/common"
	cmdBaidu "gitlab.com/yyyolu/alist-baidu/server/controllers/cmd"

	"net/http"

	"github.com/gin-gonic/gin"
)

func BaiduFileSave(c *gin.Context) {
	c.HTML(http.StatusOK, "index", gin.H{})
}

func BaiduFileAdminSave(c *gin.Context) {
	c.HTML(http.StatusOK, "indexAdmin", gin.H{})
}

func BaiduFileSaveDeal(c *gin.Context) {
	// 获取表单传递的参数
	folder := c.PostForm("folder")
	link := c.PostForm("link")
	passwd := c.PostForm("passwd")
	// 1. 用户创建专属文件夹
	// 进入/alist/user
	var paramCd []string
	paramCd = append(paramCd, "cd")
	paramCd = append(paramCd, "/alist/transfer")
	if !cmdBaidu.BaiduPCPort("BaiduPCS-Go", paramCd) {
		return
	}
	// 创建文件夹
	var paramsMkdir []string
	paramsMkdir = append(paramsMkdir, "mkdir")
	paramsMkdir = append(paramsMkdir, folder)
	if !cmdBaidu.BaiduPCPort("BaiduPCS-Go", paramsMkdir) {
		return
	}
	// 进入存储目录
	var paramCdTwice []string
	paramCdTwice = append(paramCdTwice, "cd")
	paramCdTwice = append(paramCdTwice, folder)
	if !cmdBaidu.BaiduPCPort("BaiduPCS-Go", paramCdTwice) {
		return
	}
	// 保存文件
	var paramTransfer []string
	paramTransfer = append(paramTransfer, "transfer")
	paramTransfer = append(paramTransfer, link)
	paramTransfer = append(paramTransfer, passwd)
	if !cmdBaidu.BaiduPCPort("BaiduPCS-Go", paramTransfer) {
		return
	}
	// 刷新缓存
	err := conf.Cache.Clear(conf.Ctx)
	go func() {
		if err != nil {
			common.MailSend("创建了"+folder+"文件夹,保存链接失败", false)
		} else {
			common.MailSend("创建了"+folder+"文件夹,保存链接成功", true)
		}

	}()
	c.Redirect(http.StatusMovedPermanently, "https://baidu.duskhouse.cn/")
}
func BaiduFileAdminSaveDeal(c *gin.Context) {
	// 获取表单传递的参数
	category := c.PostForm("category")
	folder := c.PostForm("folder")
	link := c.PostForm("link")
	passwd := c.PostForm("passwd")
	// 1. 用户创建专属文件夹
	// 进入/alist/user
	var paramCd []string
	paramCd = append(paramCd, "cd")
	paramCd = append(paramCd, "/alist/admin")
	if !cmdBaidu.BaiduPCPort("BaiduPCS-Go", paramCd) {
		return
	}
	// 创建文件夹
	var paramsMkdir []string
	paramsMkdir = append(paramsMkdir, "mkdir")
	paramsMkdir = append(paramsMkdir, category)
	if !cmdBaidu.BaiduPCPort("BaiduPCS-Go", paramsMkdir) {
		return
	}
	// 进入存储目录
	var paramCdTwice []string
	paramCdTwice = append(paramCdTwice, "cd")
	paramCdTwice = append(paramCdTwice, category)
	if !cmdBaidu.BaiduPCPort("BaiduPCS-Go", paramCdTwice) {
		return
	}
	// 创建文件夹
	var paramsMkdirTwice []string
	paramsMkdirTwice = append(paramsMkdirTwice, "mkdir")
	paramsMkdirTwice = append(paramsMkdirTwice, folder)
	if !cmdBaidu.BaiduPCPort("BaiduPCS-Go", paramsMkdirTwice) {
		return
	}
	// 进入存储目录
	var paramCdThree []string
	paramCdThree = append(paramCdThree, "cd")
	paramCdThree = append(paramCdThree, folder)
	if !cmdBaidu.BaiduPCPort("BaiduPCS-Go", paramCdThree) {
		return
	}
	// 保存文件
	var paramTransfer []string
	paramTransfer = append(paramTransfer, "transfer")
	paramTransfer = append(paramTransfer, link)
	paramTransfer = append(paramTransfer, passwd)
	if !cmdBaidu.BaiduPCPort("BaiduPCS-Go", paramTransfer) {
		return
	}
	// 刷新缓存
	err := conf.Cache.Clear(conf.Ctx)
	go func() {
		if err != nil {
			common.MailSend("创建了"+folder+"文件夹,保存链接失败", false)
		} else {
			common.MailSend("创建了"+folder+"文件夹,保存链接成功", true)
		}

	}()
	c.Redirect(http.StatusMovedPermanently, "https://baidu.duskhouse.cn/")
}
