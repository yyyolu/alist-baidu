package file

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/yyyolu/alist-baidu/drivers/operate"
	"gitlab.com/yyyolu/alist-baidu/server/common"
)

type MkdirReq struct {
	Path string `json:"path"`
}

func Mkdir(c *gin.Context) {
	var req MkdirReq
	if err := c.ShouldBind(&req); err != nil {
		common.ErrorResp(c, err, 400)
		return
	}
	account, path_, driver, err := common.ParsePath(req.Path)
	if err != nil {
		common.ErrorResp(c, err, 500)
		return
	}
	if path_ == "/" {
		common.ErrorStrResp(c, "Folder name can't be empty", 400)
		return
	}
	err = operate.MakeDir(driver, account, path_, true)
	if err != nil {
		common.ErrorResp(c, err, 500)
		return
	}
	common.SuccessResp(c)
}
