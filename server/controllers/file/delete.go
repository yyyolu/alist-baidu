package file

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/yyyolu/alist-baidu/drivers/base"
	"gitlab.com/yyyolu/alist-baidu/drivers/operate"
	"gitlab.com/yyyolu/alist-baidu/server/common"
	"gitlab.com/yyyolu/alist-baidu/utils"
)

type DeleteFilesReq struct {
	Path  string   `json:"path"`
	Names []string `json:"names"`
}

func DeleteFiles(c *gin.Context) {
	var req DeleteFilesReq
	if err := c.ShouldBind(&req); err != nil {
		common.ErrorResp(c, err, 400)
		return
	}
	if len(req.Names) == 0 {
		common.ErrorStrResp(c, "Empty file names", 400)
		return
	}
	for i, name := range req.Names {
		account, path_, driver, err := common.ParsePath(utils.Join(req.Path, name))
		if err != nil {
			common.ErrorResp(c, err, 500)
			return
		}
		if path_ == "/" {
			common.ErrorStrResp(c, "Delete root folder is not allowed", 400)
			return
		}
		clearCache := false
		if i == len(req.Names)-1 {
			clearCache = true
		}
		err = operate.Delete(driver, account, path_, clearCache)
		if err != nil {
			if i == 0 {
				_ = base.DeleteCache(utils.Dir(path_), account)
			}
			common.ErrorResp(c, err, 500)
			return
		}
	}
	common.SuccessResp(c)
}
