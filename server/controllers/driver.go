package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/yyyolu/alist-baidu/drivers/base"
	"gitlab.com/yyyolu/alist-baidu/server/common"
)

func GetDrivers(c *gin.Context) {
	common.SuccessResp(c, base.GetDrivers())
}
