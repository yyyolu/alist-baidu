package cmdBaidu

import (
	"bufio"
	"io"
	"os/exec"
	"strings"
)

// cmd接口
func BaiduPCPort(command string, param []string) bool {
	shell_command := exec.Command(command, param...)
	stdout, err := shell_command.StdoutPipe()
	if err != nil {
		return false
	}
	shell_command.Start()
	//创建流来接收读取内容
	reader := bufio.NewReader(stdout)
	for {
		//按行读取
		line, err := reader.ReadString('\n')
		if err != nil || err == io.EOF {
			break
		}
		// fmt.Println(line)
		if strings.Contains(line, "分享链接已失效") {
			return false
		}
	}
	shell_command.Wait()
	return true
}
