package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/yyyolu/alist-baidu/conf"
	"gitlab.com/yyyolu/alist-baidu/server/common"
)

func ClearCache(c *gin.Context) {
	err := conf.Cache.Clear(conf.Ctx)
	if err != nil {
		common.ErrorResp(c, err, 500)
	} else {
		common.SuccessResp(c)
	}
}
