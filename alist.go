package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/yyyolu/alist-baidu/bootstrap"
	"gitlab.com/yyyolu/alist-baidu/conf"
	_ "gitlab.com/yyyolu/alist-baidu/drivers"
	"gitlab.com/yyyolu/alist-baidu/model"
	"gitlab.com/yyyolu/alist-baidu/server"
)

func Init() bool {
	// 邮件配置
	bootstrap.InitMailConfig()
	bootstrap.InitConf()
	bootstrap.InitCron()
	bootstrap.InitModel()
	if conf.Password {
		pass, err := model.GetSettingByKey("password")
		if err != nil {
			log.Errorf(err.Error())
			return false
		}
		fmt.Printf("your password: %s\n", pass.Value)
		return false
	}
	server.InitIndex()
	bootstrap.InitSettings()
	bootstrap.InitAccounts()
	bootstrap.InitCache()
	return true
}

func main() {
	if conf.Version {
		fmt.Printf("Built At: %s\nGo Version: %s\nAuthor: %s\nCommit ID: %s\nVersion: %s\nWebVersion: %s\n",
			conf.BuiltAt, conf.GoVersion, conf.GitAuthor, conf.GitCommit, conf.GitTag, conf.WebTag)
		return
	}
	if !Init() {
		return
	}
	if !conf.Debug {
		gin.SetMode(gin.ReleaseMode)
	}
	r := gin.Default()
	r.LoadHTMLGlob("template/*")
	r.Static("/static", "./static")
	server.InitApiRouter(r)
	base := fmt.Sprintf("%s:%d", conf.Conf.Address, conf.Conf.Port)
	log.Infof("start server @ %s", base)
	var err error
	if conf.Conf.Scheme.Https {
		err = r.RunTLS(base, conf.Conf.Scheme.CertFile, conf.Conf.Scheme.KeyFile)
	} else {
		err = r.Run(base)
	}
	if err != nil {
		log.Errorf("failed to start: %s", err.Error())
	}
}
