package bootstrap

import (
	"github.com/robfig/cron/v3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/yyyolu/alist-baidu/conf"
)

// InitCron init cron
func InitCron() {
	log.Infof("init cron...")
	conf.Cron = cron.New()
	conf.Cron.Start()
}
