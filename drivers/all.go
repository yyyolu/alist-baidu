package drivers

import (
	"strings"

	log "github.com/sirupsen/logrus"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/123"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/139"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/189"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/189pc"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/alidrive"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/alist"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/baidu"
	"gitlab.com/yyyolu/alist-baidu/drivers/base"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/ftp"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/google"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/lanzou"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/mediatrack"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/native"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/onedrive"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/pikpak"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/quark"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/s3"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/sftp"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/shandian"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/teambition"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/uss"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/webdav"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/xunlei"
	_ "gitlab.com/yyyolu/alist-baidu/drivers/yandex"
)

var NoCors string
var NoUpload string

func GetConfig() {
	for k, v := range base.GetDriversMap() {
		if v.Config().NoCors {
			NoCors += k + ","
		}
		if v.Upload(nil, nil) != base.ErrEmptyFile {
			NoUpload += k + ","
		}
	}
	NoCors = strings.Trim(NoCors, ",")
	NoUpload += "root"
}

func init() {
	log.Debug("all init")
	GetConfig()
}
