package webdav

import (
	"net/http"
	"strings"

	"gitlab.com/yyyolu/alist-baidu/drivers/base"
	"gitlab.com/yyyolu/alist-baidu/drivers/webdav/odrvcookie"
	"gitlab.com/yyyolu/alist-baidu/model"
	"gitlab.com/yyyolu/alist-baidu/pkg/gowebdav"
	"gitlab.com/yyyolu/alist-baidu/utils"
)

func (driver WebDav) NewClient(account *model.Account) *gowebdav.Client {
	c := gowebdav.NewClient(account.SiteUrl, account.Username, account.Password)
	if isSharePoint(account) {
		cookie, err := odrvcookie.GetCookie(account.Username, account.Password, account.SiteUrl)
		if err == nil {
			c.SetInterceptor(func(method string, rq *http.Request) {
				rq.Header.Del("Authorization")
				rq.Header.Set("Cookie", cookie)
			})
		}
	}
	return c
}

func (driver WebDav) WebDavPath(path string) string {
	path = utils.ParsePath(path)
	path = strings.TrimPrefix(path, "/")
	return path
}

func init() {
	base.RegisterDriver(&WebDav{})
}
