package webdav

import "gitlab.com/yyyolu/alist-baidu/model"

func isSharePoint(account *model.Account) bool {
	return account.InternalType == "sharepoint"
}
